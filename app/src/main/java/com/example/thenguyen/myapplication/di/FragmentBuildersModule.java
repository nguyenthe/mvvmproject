package com.example.thenguyen.myapplication.di;

import com.example.thenguyen.myapplication.view.ui.ProjectFragment;
import com.example.thenguyen.myapplication.view.ui.ProjectListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract ProjectFragment contributeProjectFragment();

    @ContributesAndroidInjector
    abstract ProjectListFragment contributeProjectListFragment();

}
