package com.example.thenguyen.myapplication.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.Target;
import com.example.thenguyen.myapplication.R;
import com.glidebitmappool.GlideBitmapFactory;
import com.glidebitmappool.GlideBitmapPool;

import java.io.File;

public class BitMapPoolActivity extends AppCompatActivity {

    private ImageView imgView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bitmap_pool_layout);
        GlideBitmapPool.initialize(10 * 1024 * 1024); //max is 10MB
        init();

    }


    public void init() {
        imgView = findViewById(R.id.img_View);
        Glide.with(this).load(R.drawable.test1).into(imgView);
        findViewById(R.id.normalResource).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                normalResource(v);
            }
        });

        //normalFile
        findViewById(R.id.normalFile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                normalFile(v);
            }
        });

        //resourceOptimized
        findViewById(R.id.resourceOptimized).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resourceOptimized(v);
            }
        });
        //fileOptimized
        findViewById(R.id.fileOptimized).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileOptimized(v);
            }
        });
        //downSample
        findViewById(R.id.downSample).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downSample(v);
            }
        });

        //clearMemory
        findViewById(R.id.clearMemory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearMemory(v);
            }
        });
    }

    public void normalResource(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test1);
                bitmap1.recycle();
                Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.test2);
                bitmap2.recycle();
                Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), R.drawable.test3);
                bitmap3.recycle();
                Bitmap bitmap4 = BitmapFactory.decodeResource(getResources(), R.drawable.test4);
                bitmap4.recycle();
                Bitmap bitmap5 = BitmapFactory.decodeResource(getResources(), R.drawable.test5);
                bitmap5.recycle();
                Bitmap bitmap6 = BitmapFactory.decodeResource(getResources(), R.drawable.test6);
                bitmap6.recycle();
                Bitmap bitmap7 = BitmapFactory.decodeResource(getResources(), R.drawable.test7);
                bitmap7.recycle();
                Bitmap bitmap8 = BitmapFactory.decodeResource(getResources(), R.drawable.test8);
                bitmap8.recycle();
                Bitmap bitmap9 = BitmapFactory.decodeResource(getResources(), R.drawable.test9);
                bitmap9.recycle();
                Bitmap bitmap10 = BitmapFactory.decodeResource(getResources(), R.drawable.test10);
                bitmap10.recycle();
            }
        }).start();
    }

    public void normalFile(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Download" + File.separator + "test";
                for (int i = 1; i <= 10; i++) {
                    Bitmap bitmap = BitmapFactory.decodeFile(path + i + ".png");
                    bitmap.recycle();
                }
            }
        }).start();
    }

    public void resourceOptimized(View view) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap1 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test1);
                GlideBitmapPool.putBitmap(bitmap1);

                Bitmap bitmap2 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test2);
                GlideBitmapPool.putBitmap(bitmap2);

                Bitmap bitmap3 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test3);
                GlideBitmapPool.putBitmap(bitmap3);

                Bitmap bitmap4 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test4);
                GlideBitmapPool.putBitmap(bitmap4);

                Bitmap bitmap5 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test5);
                GlideBitmapPool.putBitmap(bitmap5);

                Bitmap bitmap6 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test6);
                GlideBitmapPool.putBitmap(bitmap6);

                Bitmap bitmap7 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test7);
                GlideBitmapPool.putBitmap(bitmap7);

                Bitmap bitmap8 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test8);
                GlideBitmapPool.putBitmap(bitmap8);

                Bitmap bitmap9 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test9);
                GlideBitmapPool.putBitmap(bitmap9);

                Bitmap bitmap10 = GlideBitmapFactory.decodeResource(getResources(), R.drawable.test10);
                GlideBitmapPool.putBitmap(bitmap10);
            }
        }).start();
    }

    public void fileOptimized(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Download" + File.separator + "test";
                    for (int i = 1; i <= 10; i++) {
                        Bitmap bitmap = GlideBitmapFactory.decodeFile(path + i + ".png");
                        GlideBitmapPool.putBitmap(bitmap);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void downSample(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Download" + File.separator + "test";
                    for (int i = 1; i <= 10; i++) {
                        Bitmap bitmap = GlideBitmapFactory.decodeFile(path + i + ".png", 100, 100);
                        GlideBitmapPool.putBitmap(bitmap);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void clearMemory(View view) {
        GlideBitmapPool.clearMemory();
    }
}
