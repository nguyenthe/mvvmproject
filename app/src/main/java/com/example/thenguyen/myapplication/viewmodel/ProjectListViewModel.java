package com.example.thenguyen.myapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.thenguyen.myapplication.service.model.Project;
import com.example.thenguyen.myapplication.service.repository.ProjectRepository;

import java.util.List;

import javax.inject.Inject;

public class ProjectListViewModel extends AndroidViewModel {

    private final LiveData<List<Project>> projectListObservable;

    @Inject
    public ProjectListViewModel(ProjectRepository projectRepository, Application application) {
        super(application);
        projectListObservable =projectRepository.getProjectList("Google");
    }

    public LiveData<List<Project>> getProjectListObservable() {
        return projectListObservable;
    }
}
