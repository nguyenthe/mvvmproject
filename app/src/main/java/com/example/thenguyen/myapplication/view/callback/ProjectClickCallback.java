package com.example.thenguyen.myapplication.view.callback;

import com.example.thenguyen.myapplication.service.model.Project;

public interface ProjectClickCallback {
    void onClick(Project project);
}
