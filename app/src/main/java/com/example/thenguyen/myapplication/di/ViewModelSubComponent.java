package com.example.thenguyen.myapplication.di;

import com.example.thenguyen.myapplication.viewmodel.ProjectListViewModel;
import com.example.thenguyen.myapplication.viewmodel.ProjectViewModel;

import dagger.Subcomponent;

@Subcomponent
public interface ViewModelSubComponent {
    @Subcomponent.Builder
    interface Builder{
        ViewModelSubComponent build();
    }

    ProjectListViewModel projectListViewModel();
    ProjectViewModel projectViewModel();

}
