package com.example.thenguyen.myapplication.view.ui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.example.thenguyen.myapplication.di.Injectable;
import com.example.thenguyen.myapplication.viewmodel.ProjectViewModel;

import javax.inject.Inject;

public class ProjectFragment extends Fragment implements Injectable {

    @Inject
    ViewModelProvider.Factory  viewModelFactory;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final ProjectViewModel viewModel = ViewModelProviders.of(this, viewModelFactory)
                                                            .get(ProjectViewModel.class);

    }
}
