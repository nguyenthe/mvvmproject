package com.example.thenguyen.myapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.example.thenguyen.myapplication.service.model.Project;
import com.example.thenguyen.myapplication.service.repository.ProjectRepository;

import javax.inject.Inject;

public class ProjectViewModel extends AndroidViewModel {


    public ObservableField<Project> project = new ObservableField<>();
    private static final MutableLiveData ABSENT = new MutableLiveData();
    {
        //noinspection unchecked
        ABSENT.setValue(null);
    }
    private final LiveData projectObserveable;
    private final MutableLiveData<String> projectID;


    @Inject
    public ProjectViewModel(ProjectRepository projectRepository,@NonNull Application application) {
        super(application);
        this.projectID = new MutableLiveData<>();
        projectObserveable = Transformations.switchMap(projectID, input -> {
           if(input.isEmpty()){
               return ABSENT;
           }

           return projectRepository.getProjectDetails("Google", projectID.getValue());
        });
    }

    public LiveData<Project> getProjectObserveable() {
        return projectObserveable;
    }


    public void setProject(Project project) {
        this.project.set(project);
    }

    public void setProjectID(String projectID) {
        this.projectID.setValue(projectID);
    }

}
