package com.example.thenguyen.myapplication.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thenguyen.myapplication.R;
import com.example.thenguyen.myapplication.service.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ReciveMessageEvent extends AppCompatActivity {

    private TextView mTvMessage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.received_message_event);
        mTvMessage = findViewById(R.id.tvMessage);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        Toast.makeText(this, " onEvent " + event.getMessage(), Toast.LENGTH_LONG).show();
        Log.e(ReciveMessageEvent.class.getName(), " content message : " + event.getMessage());
        mTvMessage.setText(event.getMessage());
    }
}
